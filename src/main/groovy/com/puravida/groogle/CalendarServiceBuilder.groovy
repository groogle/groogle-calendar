package com.puravida.groogle

import com.puravida.groogle.impl.GroovyCalendarService

class CalendarServiceBuilder {

    static CalendarService build(){
        new GroovyCalendarService()
    }

}
