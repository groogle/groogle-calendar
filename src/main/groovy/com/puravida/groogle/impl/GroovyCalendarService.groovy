package com.puravida.groogle.impl

import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.services.calendar.Calendar
import com.google.api.services.calendar.model.CalendarList
import com.google.auth.Credentials
import com.google.auth.http.HttpCredentialsAdapter
import com.puravida.groogle.CalendarService
import com.puravida.groogle.WithCalendar
import com.puravida.groogle.WithExistingCalendar

import java.util.function.Consumer

class GroovyCalendarService implements InternalService, CalendarService{

    Calendar service

    @Override
    void configure(JsonFactory jsonFactory, HttpTransport httpTransport, Credentials credentials, String applicationName) {
        def http = new HttpCredentialsAdapter(credentials)
        this.service = new Calendar.Builder(httpTransport, jsonFactory, http)
                .setApplicationName(applicationName)
                .build()
    }

    @Override
    String[] getCalendarIds() {
        def ret = []
        eachCalendarInList{
            ret.add it.id
        }
        ret as String[]
    }

    @Override
    int eachCalendarInList(Consumer<WithExistingCalendar> consumer) {
        int count=0
        CalendarList list = this.service.calendarList().list().execute()
        while( true ) {
            list.items.each {
                count++
                com.google.api.services.calendar.model.Calendar calendar =
                        service.calendars().get(it.id).execute()
                WithCalendarImpl impl = new WithCalendarImpl(service: service,calendar: calendar)
                consumer.accept(impl)
                impl.execute()
            }
            if( !list.nextPageToken )
                break
            list = this.service.calendarList().list().setPageToken(list.nextPageToken).execute()
        }
        count
    }

    @Override
    void withPrimaryCalendar(Consumer<WithExistingCalendar> consumer) {
        withCalendar('primary',consumer)
    }

    @Override
    void withCalendar(String calendarId, Consumer<WithExistingCalendar> consumer) {
        com.google.api.services.calendar.model.Calendar calendar = service.calendars().get(calendarId).execute()
        assert calendar
        WithCalendarImpl impl = new WithCalendarImpl(service: service, calendar:calendar)
        consumer.accept(impl)
        impl.execute()
    }

    @Override
    String newCalendar(Consumer<WithCalendar> consumer) {
        com.google.api.services.calendar.model.Calendar calendar = new com.google.api.services.calendar.model.Calendar()
        WithCalendarImpl impl = new WithCalendarImpl(service: service, calendar:calendar, toCreate: true)
        consumer.accept(impl)
        impl.execute()
        impl.calendar.id
    }

    @Override
    void deleteCalendar(String calendarId) {
        service.calendars().delete(calendarId).execute()
    }
}
