package com.puravida.groogle

import com.google.api.services.calendar.CalendarScopes
import spock.lang.Shared
import spock.lang.Specification

class EventsGroogleTest extends Specification{


    @Shared
    Groogle groogle

    @Shared
    String calendarId

    void setupSpec(){
        //tag::register[]
        groogle = GroogleBuilder.build {
            withOAuthCredentials {
                applicationName 'test-calendar'
                withScopes CalendarScopes.CALENDAR
                usingCredentials "src/test/resources/client_secret.json"
                storeCredentials true
            }
            register(CalendarServiceBuilder.build(), CalendarService)
        }
        CalendarService service = groogle.service(CalendarService)
        calendarId = service.newCalendar {
            it.summary = "Test calendar, can be removed"
        }
        //end::register[]
    }

    void cleanSpec(){
        CalendarService service = groogle.service(CalendarService)
        service.deleteCalendar(calendarId)
    }


    void "list events in an empty calendar"(){

        when: "list events"

        CalendarService service = groogle.service(CalendarService)

        int count = 0
        service.withCalendar calendarId, {
            it.eachEventInCalendar{
                count++
            }
        }

        then:
        count == 0
    }

    void "crud events in an empty calendar"(){

        //tag::crud[]
        when: "we create an events"

        CalendarService service = groogle.service(CalendarService)
        String eventId

        service.withCalendar calendarId, {
            eventId = it.newEvent{
                it.summary = "new event for test"
                it.start = it.end = new Date()
            }
        }

        int count
        service.withCalendar calendarId, {
            it.eachEventInCalendar{
                println  "there are some events with id $eventId"
                count++
            }
        }

        then:
        count == 1

        when: "removed the event"
        service.withCalendar calendarId, {
            it.deleteEvent(eventId)
        }

        count = 0
        service.withCalendar calendarId, {
            it.eachEventInCalendar{
                println  "there are some events"
                count++
            }
        }

        then:
        count == 0
        //end::crud[]
    }

}
