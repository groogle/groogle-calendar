= Groogle Calendar
Jorge Aguilera <jorge.aguilera@puravida-software.com>
:description: Groogle Calendar repository

This is the Groogle Calendar repository.

This project belongs to the Groogle projects group. You can review them at https://gitlab.com/groogle[Groogle projects group]

Visit https://groogle.gitlab.io
